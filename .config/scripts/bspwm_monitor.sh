#! /usr/bin/bash

export DISPLAY=:0
export XAUTHORITY=/home/i0n3l/.Xauthority

function connect(){
    xrandr --output HDMI-1 --auto --left-of eDP-1 --output eDP-1
}

  
function disconnect(){
      xrandr --output HDMI-1 --off
}

xrandr | grep "HDMI-1 connected" &> /dev/null && connect || disconnect

